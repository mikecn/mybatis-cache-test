package com.lagou;

import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.plugin.*;

import java.sql.Connection;
import java.util.Properties;

/**
 * @author lyj
 * @Title: MyPlugin
 * @ProjectName mybatis-test
 * @Description: TODO
 * @date 2021/5/2 21:37
 */
@Intercepts({ //此处可以定义多个@Signature对多个地方进行拦截，都用这个拦截器
        @Signature(type = StatementHandler.class, //指拦截哪个接口
        method = "prepare", //拦截接口内的哪个方法名
        args = {Connection.class,Integer.class}) //这是拦截方法的入参，按顺序写到这，不要多也不要少，
        // 如果方法重载，可要通过方法名和入参来确定唯一的
})
public class MyPlugin implements Interceptor {

    //每次执行操作的时候，都会进行这个拦截器的方法内的
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        System.out.println("对方法进行了增强。。。");
        return invocation.proceed();  //执行原方法
    }

    //主要是把拦截器生成一个代理放到代理器链中
    @Override
    public Object plugin(Object o) {
//        System.out.println("将要包装的目标对象："+o);
        return Plugin.wrap(o,this);
    }

    /**
     * 获取配置文件的属性
     * 插件初始化的时候调用，也只调用一次，插件配置的属性从这里设置进来
     * @param properties
     */
    @Override
    public void setProperties(Properties properties) {
        System.out.println("插件配置的初始化参数："+properties);
    }
}
