//package com.lagou.dao;
//
//import com.lagou.domain.User;
//import org.apache.ibatis.io.Resources;
//import org.apache.ibatis.session.SqlSession;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.apache.ibatis.session.SqlSessionFactoryBuilder;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.List;
//
///**
// * @author lyj
// * @Title: UserDaoImpl
// * @ProjectName mybatis-test
// * @Description: TODO
// * @date 2021/5/2 14:03
// */
//
//public class UserDaoImpl implements UserDao{
//    @Override
//    public List<User> findAll() throws IOException {
//        InputStream resourceAsStream =
//                Resources.getResourceAsStream("SqlMapConfig.xml");
//        SqlSessionFactory sqlSessionFactory = new
//                SqlSessionFactoryBuilder().build(resourceAsStream);
//        SqlSession sqlSession = sqlSessionFactory.openSession();
//        List<User> userList = sqlSession.selectList("userMapper.findAll");
//        sqlSession.close();
//        return userList;
//    }
//}
