package com.lagou.dao;

import com.lagou.domain.User;

import java.io.IOException;
import java.util.List;

/**
 * @author lyj
 * @Title: UserDao
 * @ProjectName mybatis-test
 * @Description: TODO
 * @date 2021/5/2 11:35
 */

public interface UserDao {
    List<User> findAll() throws IOException;
    User selectUserByUserId(int id);
    int updateUserByUserId(User user);
}
