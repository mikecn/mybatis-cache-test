package com.lagou.test;

import com.lagou.dao.UserDao;
import com.lagou.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author lyj
 * @Title: UserTests
 * @ProjectName mybatis-test
 * @Description: TODO
 * @date 2021/5/2 11:14
 */

public class UserTests {

    /**
     * 测试mybatis配置是否正常
     * @throws IOException
     */
    @Test
   public void testSqlSession() throws IOException {

        InputStream resourceAsStream =
                Resources.getResourceAsStream("SqlMapConfig.xml");

        SqlSessionFactory sqlSessionFactory = new
                SqlSessionFactoryBuilder().build(resourceAsStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();

        List<User> userList = sqlSession.selectList("userMapper.findAll");

        System.out.println(userList);

        sqlSession.close();
    }

//    /**
//     * 传统的Dao开发方式
//     * @throws IOException
//     */
//    @Test
//    public void testTraditionDao() throws IOException {
//        UserDao userDao=new UserDaoImpl();
//        List<User> userList = userDao.findAll();
//        System.out.println(userList);
//    }

    /**
     * 代理开发方式
     * @throws IOException
     */
    @Test
    public void testProxyDao() throws IOException {
        InputStream resourceAsStream =
                Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new
                SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        UserDao userDao = sqlSession.getMapper(UserDao.class);
        List<User> all = userDao.findAll();
        System.out.println(all);
        sqlSession.close();
    }

    /**
     * 测试mybatis缓存
     * @throws IOException
     */
    @Test
    public void testMybatisCache() throws IOException {
        InputStream resourceAsStream =
                Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new
                SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        UserDao userDao = sqlSession.getMapper(UserDao.class);
        User user = userDao.selectUserByUserId(1);
        System.out.println(user);
        sqlSession.close();
    }
}
