package com.lagou.test;

import com.lagou.dao.UserDao;
import com.lagou.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lyj
 * @Title: CacheTest
 * @ProjectName mybatis-test
 * @Description: TODO
 * @date 2021/5/2 14:58
 */

public class CacheTest {
    private SqlSession sqlSession;
    private UserDao userDao;
    @Before
    public void createSqlSession() throws IOException {
        InputStream resourceAsStream =
                Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new
                SqlSessionFactoryBuilder().build(resourceAsStream);
         sqlSession = sqlSessionFactory.openSession(true);
         userDao = sqlSession.getMapper(UserDao.class);
    }

    /**
     * 测试一级缓存
     */
    @Test
    public void firstLevelCache(){
        //第一次查询
        User user1 = userDao.selectUserByUserId(1);
        //更新操作
        User user=new User();
        user.setId(2);
        user.setUsername("333哈哈");
        userDao.updateUserByUserId(user);
        //第二次查询
        User user2 = userDao.selectUserByUserId(1);
        System.out.println(user1==user2);
    }

    /**
     * 测试二级缓存
     * @throws IOException
     */
    @Test
    public void secondLevelCache() throws IOException {
        InputStream resourceAsStream =
                Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new
                SqlSessionFactoryBuilder().build(resourceAsStream);
        //根据sqlSessionFactory产生sqlSession
        SqlSession sqlSession1 = sqlSessionFactory.openSession();
        SqlSession sqlSession2 = sqlSessionFactory.openSession();
        SqlSession sqlSession3 = sqlSessionFactory.openSession();

        UserDao userDao1 = sqlSession1.getMapper(UserDao.class);
        UserDao userDao2 = sqlSession2.getMapper(UserDao.class);
        UserDao userDao3 = sqlSession3.getMapper(UserDao.class);

        //第一次查询
        User user1 = userDao1.selectUserByUserId(1);
        System.out.println(user1);
        sqlSession1.close();

        //执行更新操作，commit
        User user=new User();
        user.setId(2);
        user.setUsername("李哲");
        user.setPassword("333");
        userDao3.updateUserByUserId(user);
        sqlSession3.commit();

        //第二次查询
        User user2 = userDao2.selectUserByUserId(1);
        System.out.println(user2);
        sqlSession2.close();

    }
}
